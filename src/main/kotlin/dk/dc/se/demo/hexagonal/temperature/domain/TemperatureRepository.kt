package dk.dc.se.demo.hexagonal.temperature.domain

import java.time.LocalDate

interface TemperatureRepository {

    fun getTemperatureByDate(date: LocalDate): Temperature?
}
