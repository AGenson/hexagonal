package dk.dc.se.demo.hexagonal.temperature.presentation.graphql

import dk.dc.se.demo.hexagonal.temperature.domain.Temperature
import dk.dc.se.demo.hexagonal.temperature.domain.TemperatureService
import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
@Profile("gqljpa")
class TemperatureQueryResolver(
        private val temperatureService: TemperatureService
): GraphQLQueryResolver {

    fun temperature(date: LocalDate?): Temperature? = this.temperatureService.getTemperatureByDate(date)
}
