package dk.dc.se.demo.hexagonal.temperature.infrastructure.jpa

import dk.dc.se.demo.hexagonal.temperature.domain.Temperature

fun TemperatureEntity.toDomain() = Temperature(
        this.temperature,
        this.date
)
