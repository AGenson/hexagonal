package dk.dc.se.demo.hexagonal.temperature.infrastructure

import dk.dc.se.demo.hexagonal.temperature.domain.TemperatureRepository
import dk.dc.se.demo.hexagonal.temperature.domain.TemperatureService
import dk.dc.se.demo.hexagonal.temperature.application.DefaultTemperatureService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class Config {

    @Bean
    fun temperatureService(temperatureRepository: TemperatureRepository): TemperatureService =
            DefaultTemperatureService(temperatureRepository)
}
