package dk.dc.se.demo.hexagonal.temperature.infrastructure.jpa

import dk.dc.se.demo.hexagonal.temperature.domain.Temperature
import dk.dc.se.demo.hexagonal.temperature.domain.TemperatureRepository
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
@Profile("httpjpa | gqljpa")
class TemperatureJpaRepository(
        private val temperatureEntityRepository: TemperatureEntityRepository
): TemperatureRepository {

    override fun getTemperatureByDate(date: LocalDate): Temperature? =
            this.temperatureEntityRepository.findByDate(date)?.toDomain()
}
