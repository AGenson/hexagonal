package dk.dc.se.demo.hexagonal.temperature.presentation.http

import dk.dc.se.demo.hexagonal.temperature.domain.Temperature
import java.time.format.DateTimeFormatter

class TemperatureObject(
        val temperature: Float,
        val date: String
) {

    companion object {
        private val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

        fun fromDomain(temperature: Temperature): TemperatureObject =
                TemperatureObject(
                        temperature.temperature,
                        temperature.date.format(formatter)
                )
    }
}
