package dk.dc.se.demo.hexagonal.temperature.infrastructure.json

import dk.dc.se.demo.hexagonal.temperature.infrastructure.json.config.readValue
import com.fasterxml.jackson.databind.ObjectMapper
import dk.dc.se.demo.hexagonal.temperature.domain.Temperature
import dk.dc.se.demo.hexagonal.temperature.domain.TemperatureRepository
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import org.springframework.util.ResourceUtils
import java.time.LocalDate

@Component
@Profile("httpjson")
class TemperatureJsonRepository(
        private val objectMapper: ObjectMapper
): TemperatureRepository {

    private val filename: String = "classpath:data.json"

    private fun fetchJson(): String = ResourceUtils.getFile(this.filename).readText(Charsets.UTF_8)

    override fun getTemperatureByDate(date: LocalDate): Temperature? {
        val temperatures: List<TemperatureEntity> = this.objectMapper.readValue(this.fetchJson())

        return temperatures.find { temp -> temp.date == date }?.toDomain()
    }
}
