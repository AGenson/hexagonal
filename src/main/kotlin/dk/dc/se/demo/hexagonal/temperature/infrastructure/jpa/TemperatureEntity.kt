package dk.dc.se.demo.hexagonal.temperature.infrastructure.jpa

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "temperature")
class TemperatureEntity(
        var temperature: Float,
        var date: LocalDate
){

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1
}
