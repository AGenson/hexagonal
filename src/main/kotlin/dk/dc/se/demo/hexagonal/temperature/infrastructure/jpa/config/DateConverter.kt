package dk.dc.se.demo.hexagonal.temperature.infrastructure.jpa.config

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter(autoApply = true)
class DateConverter: AttributeConverter<LocalDate, String> {

    private val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    override fun convertToDatabaseColumn(attribute: LocalDate?): String? =
            attribute?.format(this.formatter)

    override fun convertToEntityAttribute(dbData: String?): LocalDate? =
            dbData?.let { LocalDate.parse(it, this.formatter) }
}
