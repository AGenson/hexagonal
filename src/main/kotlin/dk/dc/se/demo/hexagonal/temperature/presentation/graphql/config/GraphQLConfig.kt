package dk.dc.se.demo.hexagonal.temperature.presentation.graphql.config

import graphql.schema.GraphQLScalarType
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
@Profile("gqljpa")
class GraphQLConfig {

    @Bean
    internal fun localDateScalarType(
            localDateScalarTypeFactory: LocalDateScalarTypeFactory
    ): GraphQLScalarType? = localDateScalarTypeFactory.getObject()
}
