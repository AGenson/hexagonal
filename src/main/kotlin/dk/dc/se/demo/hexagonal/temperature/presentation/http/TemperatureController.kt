package dk.dc.se.demo.hexagonal.temperature.presentation.http

import dk.dc.se.demo.hexagonal.temperature.domain.TemperatureService
import org.springframework.context.annotation.Profile
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
@RequestMapping("/")
@Profile("!gqljpa")
class TemperatureController(
        private val temperatureService: TemperatureService
) {

    @GetMapping("/temperature")
    fun getTemperature(
            @RequestParam(required = false)
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            date: LocalDate?
    ): ResponseEntity<TemperatureObject> = this.temperatureService.getTemperatureByDate(date)
            ?.let { TemperatureObject.fromDomain(it) }
            ?.let { temp -> ResponseEntity(temp, HttpStatus.OK) }
            ?: ResponseEntity(HttpStatus.NOT_FOUND)
}
