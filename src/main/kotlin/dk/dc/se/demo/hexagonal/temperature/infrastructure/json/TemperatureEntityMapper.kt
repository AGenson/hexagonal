package dk.dc.se.demo.hexagonal.temperature.infrastructure.json

import dk.dc.se.demo.hexagonal.temperature.domain.Temperature

fun TemperatureEntity.toDomain() = Temperature(
        this.value,
        this.date
)
