package dk.dc.se.demo.hexagonal.temperature.application

import dk.dc.se.demo.hexagonal.temperature.domain.Temperature
import dk.dc.se.demo.hexagonal.temperature.domain.TemperatureRepository
import dk.dc.se.demo.hexagonal.temperature.domain.TemperatureService
import java.time.LocalDate

class DefaultTemperatureService(
        private val temperatureRepository: TemperatureRepository
): TemperatureService {

    override fun getTemperatureByDate(date: LocalDate?): Temperature? =
        if (date != null) this.temperatureRepository.getTemperatureByDate(date)
        else this.temperatureRepository.getTemperatureByDate(LocalDate.now())
}
