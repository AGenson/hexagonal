package dk.dc.se.demo.hexagonal.temperature.infrastructure.jpa

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
interface TemperatureEntityRepository: JpaRepository<TemperatureEntity, Long> {

    fun findByDate(date: LocalDate): TemperatureEntity?
}
