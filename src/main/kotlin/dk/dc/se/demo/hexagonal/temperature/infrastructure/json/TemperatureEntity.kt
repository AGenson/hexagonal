package dk.dc.se.demo.hexagonal.temperature.infrastructure.json

import java.time.LocalDate

class TemperatureEntity(
        var value: Float,
        var date: LocalDate
)
