package dk.dc.se.demo.hexagonal.temperature.domain

import java.time.LocalDate

open class Temperature(
        var temperature: Float,
        var date: LocalDate
)
