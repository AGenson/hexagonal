package dk.dc.se.demo.hexagonal.temperature.presentation.graphql.config

import graphql.language.StringValue
import graphql.schema.*
import org.springframework.beans.factory.FactoryBean
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Component
class LocalDateScalarTypeFactory: FactoryBean<GraphQLScalarType> {

    override fun getObject(): GraphQLScalarType? = GraphQLScalarType.newScalar()
            .name("LocalDate")
            .description("Kotlin LocalDate Scalar")
            .coercing(object: Coercing<LocalDate, String> {

                private val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

                @Throws(CoercingParseValueException::class)
                override fun parseValue(input: Any): LocalDate {
                    if (input !is String) throw CoercingParseValueException("Incorrect type")
                    try {
                        return input.let { LocalDate.parse(it, this.formatter) }
                    } catch (e: IllegalArgumentException) {
                        throw CoercingParseValueException(e)
                    }
                }

                @Throws(CoercingParseLiteralException::class)
                override fun parseLiteral(input: Any): LocalDate {
                    if (input !is StringValue) throw CoercingParseLiteralException("Incorrect type")
                    try {
                        return input.let { LocalDate.parse(it.value, this.formatter) }
                    } catch (e: IllegalArgumentException) {
                        throw CoercingParseLiteralException(e)
                    }
                }

                @Throws(CoercingSerializeException::class)
                override fun serialize(dataFetcherResult: Any): String =
                        if (dataFetcherResult is LocalDate)
                            dataFetcherResult.format(this.formatter)
                        else
                            throw CoercingSerializeException("Incorrect type")
            })
            .build()


    override fun getObjectType(): Class<*>? = GraphQLScalarType::class.java

    override fun isSingleton(): Boolean = true
}
