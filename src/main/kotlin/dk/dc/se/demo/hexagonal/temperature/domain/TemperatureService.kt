package dk.dc.se.demo.hexagonal.temperature.domain

import java.time.LocalDate

interface TemperatureService {

    fun getTemperatureByDate(date: LocalDate?): Temperature?
}
