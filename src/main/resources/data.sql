INSERT INTO temperature (date, temperature) VALUES ('2022-02-23', 4);
INSERT INTO temperature (date, temperature) VALUES ('2022-02-24', 2.7);
INSERT INTO temperature (date, temperature) VALUES ('2022-02-25', 2);
INSERT INTO temperature (date, temperature) VALUES ('2022-02-26', 3.2);
INSERT INTO temperature (date, temperature) VALUES ('2022-02-27', 1.6);
INSERT INTO temperature (date, temperature) VALUES ('2022-02-28', 0.6);
INSERT INTO temperature (date, temperature) VALUES ('2022-03-01', -9);
